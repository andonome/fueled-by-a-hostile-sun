# Fueled by a Hostile Sun

## License

[                                                    ](http://creativecommons.org/licenses/by/4.0/)
 Fueled by a Hostile Sun SRD 

by [Gabriel Relich, the Martian Muckraker](https://muckraker.itch.io/) 

is licensed under a 

[Creative Commons Attribution 4.0 International License](http://creativecommons.org/licenses/by/4.0/).

 

**Game Design Goals:** This RPG . . . 

is **rules-lite**, yet still a **simulation** with game depth (as opposed to being a creative writing exercise, which some rules-lite RPGs border on). 

- has an **elegant resource currency system** which every other game mechanic connects to in some way.
- Replaces endless lists of spells / equipment with a **unified alchemy system.**
- Allows for **creature collecting**, without having complicated creature stat blocks.
- . . . and it does it while exploring a **non-violent yet brutal premise** – surviving on strange worlds, in the void of space

# Overview & Fast Rule References

## High Level – Player Basic Rules

·     **The itch.io has a one-page quick reference**

·     **Build Character:** **XXXXX** has character creation and archetypes to choose from.

·     **Roll Dice: XXXXX** Has a chart summarizing what the stats can do. Roll under on d20 is a success. Rolling your stat exactly is a crit success. (“Right on Target!” or “Crit!”)

·     **Get Currency: XXXXX** Has a chart that summarizes how any resources you mine on a successful mine action. (Mining at deposits is key to survival)

·     **Spend Currency: XXXXX** summarizes the currency tree.  **XXXXX** shows what potions cost in resources.

## High Level - Creatures

The party will regularly tame and cooperate with **alien beasts.** To keep the zoology simple, most beasts are summarized as follows:

·     A **buff** the beast gives its tamer.

·     A **pool of stat points** that can be spent to reroll a check for a given stat.

·     A **Basic Resource** it uses as **food** to recharge its pool of stat points.

·     An **Elite Resource** it can make, if given the correct Basic Resources.

## High Level – Game Master

**This is a rules-light exploration focused game.** As such, there will be lots of grey area for GM interpretation. Trust your gut, rulings not rules!

## Hostile Environments

In **Hostile Environments**, the game is played in rounds. 

The goal of hostile environment actions will often be to find **Game Currencies,** including **Basic Resources** used to tame creatures and make potions, **Clue Points** to convince others of the truth of what is happening in a situation and, in longer campaigns, **Elite** and **Divine Resources.** 

1. **In Phase 1** of the round each player can move up to their speed in squares and take one action, generally from the abovementioned chart on p24.
2. **During phase 2** of the round, you, the GM will roll dice based on the description of the environment in the chapter on Hostile Environments. Consider that description to be your “monster stat block” (only the players fight against nature, not any one creature.) 
3. **In the last phase** before starting a new round, you roll an Opportunity Die. The Round # and the **Risk Level** (difficulty level) of the environment determine how big that die is. (p25)

#  Building an Explorer

1. **Stats** –Stamina is set by your Archetype. For the remaining six stats you can assign 71 points as you see fit. A solid standard array (that adds to 71) is 16, 14, 13, 12, 9 & 7. 
2. **Pick an Archetype** – Your archetype determines how many recipes you can know, what dangers you are best at resisting, your stamina, movement and what pets/bots you can have. 
3. *(optional)* **Pick Diplomatic Keywords** – During the NPC interaction parts of the game, different NPCs will treat you differently based on your social class, methods of persuasion and other factors. See p43

## Explorer Action Stats

·     **Might** - most physical tasks, Mines **Odd Ore [OO]**

·     **Agility** - for dodging, acrobatics and the like, Mines **Arcane Algae [AA]**

·     **Focus** - to keep cool in intense situations, investigate, and resist mind effects, mines **Strange Sap [SS]**

·     **Tech** - interpret technology / evidence, mines **Molten Minerals [MM]**)**,** 

·     **Compassion** - to befriend alien creatures w/ compassion, diplomacy, mines **Helpful Herbs [HH]**) 

·     **Dominance -** to tame w/ dominance or to intimidate. Some creatures respond to compassion, some respond to dominance.

Rolling your stat or under is a success. 

## Stamina and resists

The seventh stat, stamina, doubles as your “HP.” As you take stamina damage your explorer is getting closer and closer to death. 

Your Current Stamina is used for three kinds of resists – **Elemental Resists**, **Pest Resists** and **Disease Resists**. Rolling equal or under your Current Stamina can negate a negative effect from one of these categories. Different classes, however, have points in these different resists.  When failing the stamina check for a given resist type, they can spend 1 of those matching points to reroll the failed check. These recharge between game sessions.

As you lose stamina it will become harder to make your resistance rolls – leading you to lose even more stamina.

## Archetype Table Terms

·     **Recp.** The max number of alchemy recipes your character can know how to make. 

o  A recipe can be forgotten to learn a new one. It costs 1d2 to invent a new recipe. 

·     **Resists** – Many environmental effects can be resisted if the player rolls against their current stamina (not starting stamina) and succeeds. These resists come in three types, **Disease Resist (DR)**, **Elemental Resist (ER)** and **Pest Resist (PR).** If after the initial stamina roll you have failed you can spend a point from your classes matching pool to reroll to check, OR you can spend them on a 1-to-1 basis to change the result of the first roll. 

·     **Stm.** – starting Stamina.

·     **Pet Stat** –The stat that determines the max number of pets you can own.

·     **Bot Stat:** The stat that determines the max number bots or inventions you can own.

·     **Mov** – how many squares you can move in one round whilst in a hostile environment. 

o  Values in ( ) are bonus movement points that can be spent for extra movement. They recharge between game sessions.

·     **Level** – Unlocking an Elite Currency raises the party to Level 2. Unlocking one of the Divine Currencies raises the party to level 3.

# Archetypes

## Aerospace Doctor

|               | **Recp.**     | **Stm.**    | **DR**          | **ER**        | **PR**      | **Mov** |
| ------------- | ------------- | ----------- | --------------- | ------------- | ----------- | ------- |
| **Level 1**   | 2 +1 medical  | 12          | 2               | 0             | 1           | 5       |
| **Level 2**   | 3 +2 medical  | 13          | 3               | 0             | 2           | 5       |
| **Level 3**   | 4 + 3 medical | 14          | 3               | 1             | 3           | 6       |
|               |               |             |                 |               |             |         |
| **Pet Stat:** | Compassion    | <12, 0 pets | 13-14, 1 pets   | 15-16, 2 pets | 16+, 3 pets |         |
| **Bot Stat:** | Tech          | <13, 0 bots | 14 - 17, 1 bots | 18+, 2 bots   |             |         |

 

The doctor can **learn many recipes**, especially medically related ones. **This can be helpful in adventures where finding and utilizing disease-specific medicines is important.**  The Doctor can also have both biological and bot minions. 

While most characters can only use the **Rally** action to provide stable healing to another given character once a day, the doctor can do so twice for any given character. 

## Beast Rider

|               | **Recp.**               | **Stm.**    | **DR**        | **ER**        | **PR**        | **Mov**     |
| ------------- | ----------------------- | ----------- | ------------- | ------------- | ------------- | ----------- |
| **Level 1**   | 1                       | 13          | 0             | 0             | 2             | 6 (4)       |
| **Level 2**   | 2                       | 14          | 0             | 1             | 3             | 7 (5)       |
| **Level 3**   | 3                       | 15          | 0             | 2             | 3             | 7 (6)       |
|               |                         |             |               |               |               |             |
| **Pet Stat:** | Compassion or Dominance | <10, 1 pets | 10-12, 2 pets | 13-14, 3 pets | 15-16, 4 pets | 17+, 5 pets |

Describe your ridable steed. If you are with your steed, you can spend points from the parentheses for extra movement. These extra move points recharge between Hostile Environments, provided you have at least half of your Stamina. Your rideable pet does **not** count against your pet total. All your stats, including Mov, decrease by 2 if you are separated from your pet.

The key strength of the beast rider is their **mobility** and their ability to own numerous pets at the same time. In exchange, they do not know how to tame, purchase or control bots. You are not impacted by standard difficult terrain and you are also not impacted by **one** of the following due to your steed’s favored habitat: Elemental Difficult Terrain, Aquatic Difficult Terrain, Toxic Difficult Terrain. OR instead, you may have a climb speed of three. 

## Chemistry Professor

|               | **Recp.**                          | **Stm.**    | **DR**          | **ER**        | **PR**      | **Mov** |
| ------------- | ---------------------------------- | ----------- | --------------- | ------------- | ----------- | ------- |
| **Level 1**   | 3 + 1 related to favored  resource | 14          | 1               | 1             | 1           | 5       |
| **Level 2**   | 4 + 2 related                      | 15          | 2               | 2             | 2           | 6       |
| **Level 3**   | Unlimited                          | 16          | 2               | 0             | 1           | 6       |
|               |                                    |             |                 |               |             |         |
| **Bot Stat:** | Tech                               | <13, 0 bots | 14 - 15, 1 bots | 16-17, 2 bots | 18+, 3 bots |         |
| **Pet Stat:** | Compassion or Dominance            | <12, 0 pets | 13-15, 1 pets   | 16-17, 2 pets | 18+, 3 pets |         |

 

The chemist can have both biological and bot minions and is able to know the most alchemical recipes at the same time.  They are also a hardy bunch, having a high starting stamina track. **Chemists are also allowed to pick a favored resource.** The chemist has bonus slots for recipes, but these recipes must include the favored resource. Once per session the chemist can make a recipe involving that resource for a resource currency cost that is one less. (So, if Molten Minerals is the favored resource, and the recipe normally costs 3 MM, once per session Chemist can make it for 2 MM)

## Hunter (Alt. Beast Rider)

|               | **Recp.** | **Stm.**    | **DR**        | **ER**        | **PR**      | **Mov** |
| ------------- | --------- | ----------- | ------------- | ------------- | ----------- | ------- |
| **Level  1**  | 1         | 13          | 0             | 0             | 2           | 6 (4)   |
| **Level  2**  | 2         | 14          | 0             | 1             | 3           | 7 (4)   |
| **Level  3**  | 3         | 15          | 0             | 2             | 3           | 7 (5)   |
|               |           |             |               |               |             |         |
| **Pet Stat:** | Might     | <10, 0 pets | 10-12, 1 pets | 13-14, 3 pets | 15+, 4 pets |         |

When confronting creatures, your robotic suit enhances but still depends on your Might. It is this might you use to assert yourself as alpha. 

When taming creatures roll against your Might. If the species in question is a Dominant species, increase your might stat for this check by 1. Once per day, you can choose to automatically succeed on a Might roll by overclocking your suit. 

Evolved do not automatically come with a rideable pet like the Beast Rider – you ARE the beast. Thus, you cannot be separated from your bonus speed, though your movement improvement track is lower than the Standard Beast Rider and you do not have a favored terrain. You have a lower number of max pets than the Beast Rider.

##  Priest (Alt. Doctor)

|               | **Recp.**  | **Stm.**    | **DR**          | **ER**        | **PR**      | **Mov** |
| ------------- | ---------- | ----------- | --------------- | ------------- | ----------- | ------- |
| **Level  1**  | 2          | 12          | 2               | 0             | 1           | 5       |
| **Level  2**  | 3          | 13          | 3               | 0             | 2           | 5       |
| **Level  3**  | 4          | 14          | 3               | 1             | 3           | 6       |
|               |            |             |                 |               |             |         |
| **Pet Stat:** | Compassion | <12, 0 pets | 13-14, 1 pets   | 15-16, 2 pets | 16+, 3 pets |         |
| **Bot Stat:** | Tech       | <13, 0 bots | 14 - 17, 1 bots | 18+, 2 bots   |             |         |

 

The Priest does not have the bonus recipes as the doctor and can only do **Rally** for stable healing for any given character once a day (like most characters). However, the Priest has two abilities the Doctor lacks. 

If the Priest has a **pool of healing points** equal to the Priest’s level +3 that recharge daily. With a spirited song of prayers, the Priest can at will bestow newfound hope and these points to any player(s) within three squares of the Priest’s current location. The Priest also has a **confessional’s insight** into human evil. When using Dominance to detect lies, the Priest can roll twice and take the better result.

## Roboticist

|               | **Recp.** | **Stm.**    | **DR**        | **ER**        | **PR**        | **Mov**    |
| ------------- | --------- | ----------- | ------------- | ------------- | ------------- | ---------- |
| **Level  1**  | 2         | 12          | 1             | 2             | 0             | 5          |
| **Level  2**  | 2         | 13          | 2             | 3             | 0             | 6          |
| **Level  3**  | 3         | 14          | 3             | 3             | 1             | 7          |
|               |           |             |               |               |               |            |
| **Bot Stat:** | Tech      | <10, 0 bots | 10-12, 2 bots | 13-14, 3 bots | 15-16, 4 bots | 17+ 5 bots |

 

The roboticist is focused on bots and can have more than any other Archetype. They can also buy bots at a **discount** of one blockchain. 

The starting number of uses from inventions you create (p37) is 2 instead of 1.

 

## Smuggler (alt. Roboticist)

|               | **Recp.** | **Stm**     | **DR**        | **ER**        | **PR**        | **Mov**    |
| ------------- | --------- | ----------- | ------------- | ------------- | ------------- | ---------- |
| **Level  1**  | 2         | 12          | 1             | 2             | 0             | 6          |
| **Level  2**  | 2         | 13          | 2             | 3             | 0             | 6          |
| **Level  3**  | 3         | 14          | 3             | 3             | 1             | 7          |
|               |           |             |               |               |               |            |
| **Bot Stat:** | Dominance | <10, 0 bots | 10-12, 2 bots | 13-14, 3 bots | 15-16, 4 bots | 17+ 5 bots |

 

This version of the Roboticist uses *Dominance* as its Bot Stat. Instead of the Roboticist’s blockchain discount, a Smuggler at start of each game session gains 1d3 of a chosen Resource Currency, which represents their connections to the black market.  

If encountering a wild bot (usually from the Ancient Space Empire) instead of Tech, Dominance can be used to tame. 

The use of Dominance reflects that their knowledge of robotics comes from their criminal past. 

# Game Modes & Goals

The Game has three main modes. Each of these will have will be explained in greater detail ahead. 

- **General Exploration**
- **Hostile Environment Exploration**
- **Diplomatic Roleplay**

In **general exploration** the characters are in an environment that is not actively trying to kill them. In a **hostile environment**, they are in an environment that taxes the strength of their spacesuit. Each round could bring disaster. 

The game can measure the success of the players in one of two ways (or both): either the collection of **evidence points**, or in getting close to unlocking the **Prime Matter.**

## Game Goal Option 1: Evidence Points

- **GM:** Invent 3- 5 NPCs. Know what secrets each are hiding and how receptive they will be to nosey PCs. (and what they can be bribed with)
- Know where clues can be found and assign **Evidence Points** for each.
- Some NPCs may only come to trust the party after they have accumulated a certain amount of Clue Points. Better clues → Better Trust
- The scenario should have **different endings** based on amount of EP found. A weak case may cause delay from authorities, whereas lots of EP makes a strong case that ensures quick action and full success. Getting all EP on something of importance might get the party noticed by the **Council of Seven** (see Lore).

 

## Game Goal Option 2: Prime Matter

·     **GM:** Have a map (even if just a sketch of areas) where different resources can be looked for & know location of hazards / alien creatures.

·     Introduce the resource currency tree pathway (p32) to the **Prime Matter** to the players. Tell them that that pathway should be considered their “Quest Window” – *it tells what goal they should be working towards.*

·     The planet should have **events** happening on it – NPCs and story matter!

# Hostile Environments

When in a **hostile environment** the PCs must remain in their spacesuits.  Should the party run out of **opportunity** conditions escalate in the environment beyond the limits of their suit. An explorer’s suit will (barely) keep them alive, but they will lose 1d8 stamina. The goal of being in a hostile environment is usually to find something, often a **quest objective**, **resources,** a **lifeform** or **evidence points.** Often players will have to weigh risk vs reward. In a hostile environment, the game is played out rounds. 

## Hostile Environment Round - Summary

### **Player Turns**

**Impacts Phase:** Role to resist environment

**Hero Phase:** Take actions

**The Spread Phase:** GM sees if more squares receive elevated impact

**Opportunity Drain Phase:** Roll the Opportunity Die

## Hostile Environment Round - Detail

**Player Turns**

1. **Impacts** **Phase** - Players to roll to resist environmental effects (decided by environment description), if needed. Check is usually tested against Stamina and defined as one of the three resist types. Risk is higher in squares with **elevated impact.** 
2. **Hero Phase –** Each player can move and take an action on the table below (p24) if they successfully test (roll under) the appropriate stat. If they do not move, they can take a second action.
3. **The Spread** **Phase**– Often hostile environments have particular squares that are either completely uninhabitable or subject those in them to the worse effects of an **elevated impact**. The GM rolls on a **situation specific table** (see chapter giving “Ten Sample Hostile Environments”) to determine how many more squares are impacted by this enhanced uninhabitableness. Roll for **cave-ins** or the like if applicable.
4. **Opportunity Die Phase** - Hostile environments are not indefinitely sustainable.  If players run out of **opportunity**, they automatically **lose 1d8 stamina** and their space suits **begin to fail**. The players run out of opportunity if, during the **opportunity roll,** the GM rolls a one. The size of the opportunity die depends on what **risk level** the environment is, and what round it is. GMs can use p25 to see what to roll. One opportunity die is rolled during this phase each round. 

**Failing Space Suits** – If Opportunity runs out, then on each round thereafter all players must test Stamina or lose 1d6 stamina points. **Archetype resist points cannot be used on this roll.**

On the following table the stats are paired with common actions for hostile environment exploration.  Items with a 🔎 may reward **evidence points.** 

 



| **Stat** (Mines)    | **Moves**                                                    |                                                              |                                                              |
| ------------------- | ------------------------------------------------------------ | ------------------------------------------------------------ | ------------------------------------------------------------ |
| **Might** (OO)      | **Clear rubble**, ending  difficult terrain or removing a block (diff. terrain can either -1 speed or  cost double movement) | **Push** teammate one square. If successful, this is a free action. If fails, it uses your action. | **Build barrier  in square.** In that square all rolls  to resist danger / negative effects can be rolled twice |
| **Agility** (AA)    | **Move over tough  terrain** at full speed (still allows player to take another action). -1  Stamina on fail | **Move stealthily**, avoiding  detection                     | **Activate Quest-  Specific Objective** **🔎** (varies) Some  quest specific objectives are only possible if EP is **tapped**. |
| **Focus** (SS)      | **Investigate,** **🔎** find out more details about a clue  depicted on map and possibly produce *EP* | **Observe**, know dangers  in all adjacent squarees          | **Activate Quest-  Specific Objective** **🔎** (varies) Some  quest specific objectives are only possible if EP is **tapped**. |
| **Tech** (MM)       | **Hack**, 🔎 opening a  digitally locked door, or gaining access to a computer system, may gain *EP* | **Understand  Found Tech,** gaining the party a limited use power up | **Activate Quest-  Specific Objective** **🔎** (varies) Some  quest specific objectives are only possible if EP is **tapped**. |
| **Compassion** (HH) | **Aid,** raising stat of  adjacent ally by +2 for a round    | **Rally,** adj. teammate they  gain 2 temp stamina (till scene end) | **Tame creature** **🔎** (different  creatures will respond to different taming styles). Some creatures may grant *EP*  if they have insight to the planet’s crisis. |
| **Dominance**       | **Use Explosive** (3 MM can  create) without injury. Consequences  of failure depend on explosive. | **Snap Out of It** – free teammate  of a mind effect         | **Tame creature** **🔎** (different  creatures will respond to different taming styles). Some creatures may grant *EP*  if they have insight to the planet’s crisis. |

 



## Risk Levels and Losing Opportunity

Hostile Environments can have one of 4 Risk Levels. The Risk Level and round number determines the size of the opportunity die.

The player’s suits **start to fail** if a 1 is rolled. If so, they automatically lose 1d8 stamina that round. Each round thereafter the players must test Stamina to avoid taking another d6 damage to stamina.

**Optional:** All players lose 2 stamina if a “2” is rolled. 

| **Round**                   | **RL  1** | **RL  2** | **RL  3** | **RL** **💀** |
| --------------------------- | --------- | --------- | --------- | ------------ |
| **1**                       | D100      | D20       | D20       | D12          |
| **2**                       | D20       | D20       | D12       | D10          |
| **3**                       | D20       | D12       | D10       | D8           |
| **4**                       | D12       | D12       | D8        | D6           |
| **5**                       | D12       | D10       | D6 . . .  | D4 . . .     |
| **6**                       | D10       | D10       | D6 . . .  | D4 . . .     |
| **7**                       | D10       | D8        | D6 . . .  | D4 . . .     |
| **8**                       | D8        | D8        | D6 . . .  | D4 . . .     |
| **9**                       | D8        | D6 . . .  | D6 . . .  | D4 . . .     |
| **10**                      | D6 . . .  | D6 . . .  | D6 . . .  | D4 . . .     |
| **11**                      | D6 . . .  | D6 . . .  | D6 . . .  | D4 . . .     |
| **12**                      | D6 . . .  | D6 . . .  | D6 . . .  | D4 . . .     |
| **13**                      | D6 . . .  | D6 . . .  | D6 . . .  | D4 . . .     |
| **14**                      | D6 . . .  | D6 . . .  | D6 . . .  | D4 . . .     |
| **15**                      | D6 . . .  | D6 . . .  | D6 . . .  | D4 . . .     |
| **16**                      | D6 . . .  | D6 . . .  | D6 . . .  | D4 . . .     |
| **Suits  hold to round 5?** | 75%       | 68%       | 57%       | 45%          |
| **hold  to round 7?**       | 60%       | 54%       | 40%       | 25%          |

# Resource Currencies & Wealth

**Game currencies allow us to abstract amounts of resources the characters have.** There are probably thousands of medically useful herbs to be found in the galaxy. The overall state of the stores of the group is abstracted into the game currency “Helpful Herbs.”

In general, one **Blockchain** is wealth equal to the **universe-average cost** of any one of these game currencies. But the fact that on average, across the universe, one Blockchain can buy sap supplies equal to one Strange Sap, doesn’t mean that on your local planet there could be a shortage of sap, perhaps causing one strange sap to be bought for four Blockchain!

## The Base Resource Currencies

The **Base Resource Currencie**s each have a generic use, that all adventurers know. Players will eventually build a list of recipes that allows for more interesting ways to “spend” these currencies. Furthermore, each currency can be used as bait for taming different creatures (p38). 

Resource management will be important as not all resources are easy to find in any given environment.  These generic uses do not need to be premade – the raw resources are utilized at the time the action is done. 

- **Molten Minerals** – Two molten minerals can     make an **explosive** that can blast through most barriers.
- **Odd Ore** – Two Odd Ore can construct any     **small simple structure.** (levers, ramps, wheels . . . )
- **Strange Sap** – Two strange Sap can create     a **stimulant**. It allows a     reroll of any check in a hostile environment. (You can see if your first die roll     fails before using the stimulant). Using     this stimulant causes you to lose two Stamina if you succeed or 1d6     stamina if you fail. 
- **Arcane Algae** – Two arcane algae can make     a **lure** that will cause creatures to come closer. 
- **Helpful Herbs** – Two helpful herbs can be     used to make a **first aid tonic** that can heal 1d6 stamina. 

## Mining Base Resource Currencies

To mine, a player must first find a **deposit** of the desired resource. GMs should usually include 2-3 deposits in a hostile environment scene. They can also provide specific points in the narrative that automatically reward a mine opportunity. 

As noted earlier, each of the Basic Resources is paired with a specific stat. On the given index chard character sheet, the resources are paired with their appropriate stats. **When on or adjacent to a square with a resource deposit, players can spend either an action or both their movement and action to mine.**  The amount they mine depends on whether they choose to devote their whole turn to it, and which resource they are mining. On a successful mine roll the player will roll a die sized based on the table on the next page.

| **Mined  Resource (Stat)**       | **Single Action** | **Whole Turn**   **(No movement)** | **Crit** (Regardless of time spent) |
| -------------------------------- | ----------------- | ---------------------------------- | ----------------------------------- |
| **Odd Ore (Might)**              | 1d2               | 1d4                                | 4                                   |
| **Molten Minerals**   **(Tech)** | 1d2               | 1d4                                | 4                                   |
| **Arcane Algae (Agility)**       | 1d4               | 1d6                                | 6                                   |
| **Strange Sap (Focus)**          | 1d4               | 1d6                                | 6                                   |
| **Helpful Herbs (Compassion)**   | 1d4               | 1d6                                | 6                                   |

 

## Elite Currencies

*Longer Campaigns Only*

In a longer, sandbox campaign it can be useful to have **“gated” areas** – locations players want to eventually get to but need to solve a problem first to get there. The three **Elite Currencies** allow for this gating to be built into the currency system.

**GMs** – give players have freedom as to how they will acquire the elite currency. Will they bargain with the local noble to gain access to a creature he has to convert resources? Find their own resources, at great risk? Will they buy some of the base currencies they need to make the elite currencies, or will they try and find them all?

To make these currencies the players will need to collect enough of the associated base currency and tame a creature that can produce it.

- **Mithril (2 OO + 2 MM + Alien Aid)** – Named for the legendary substance of old, this rumored metal is needed to reinforce space suits to explore **Plasma.** Without this reinforcement, areas defined as Plasma environments are functionally off limits to players. 2 Mithril can reinforce 3 suits. (Nothing can be made with 1 Mithril) 
- **Merciful Elixir (4 HH + Alien Aid)** – Certain plague worlds may be unlivable (or come with a high % risk of disease debuffs) unless players can inoculate themselves first.  Two Merciful Elixir can make 3 vaccine doses.
- **Mutagen (3 SS + 3 AA + Alien Aid)** – Certain stars and crystals give off **Zenoradiation** which will cause instant death unless players can use mutagen to strengthen their DNA first. The stabilization drug made from 2 Mutagen can be given to three PCs. Mutagen can also be used to give extra powers to some pets.

## Divine Currencies

*Very Long Campaigns Only*

Divine Resources unlock a special stage in the game. The players can now functionally **forge new lifeforms and construct bots** of their own choosing. 

 

- **ExoDNA** (2 Mutagen + 1 Elixir + alien aid) – used to resurrect **fossils** and make **Reborn Creatures**
- **BioMetal (**2 Mithiril + 1 Elixir + alien aid)– Used to reconstruct **ancient blueprints** to make **BioBots**

You can use one unit of the above currencies, combined with additional basic resources to produce a new lifeform. This takes 2d12 weeks of work.  

**GMs** – you can allow wild experimentation amongst players or require the finding of an Ancient Blueprint (for Bots) or an Ancient Fossil (for Exo DNA) which will set what Basic Resources are needed in addition to the requisite ExoDNA or BioMetal.

These lifeforms have stats like a player character does. All stats start at 6 and can be raised to 16 by adding the below to the creation recipe.  

| **Resource Currency**                            | **Stat Increase**                                            |
| ------------------------------------------------ | ------------------------------------------------------------ |
| 1 MM                                             | +5 Might or Stamina                                          |
| 1 OO                                             | +5 Stamina or Tech                                           |
| 2 AA                                             | +5 Agility or Compassion                                     |
| 2 SS                                             | +5 Focus or Dominance                                        |
|                                                  |                                                              |
| **Biometal Bots** cannot have a compassion  stat | **ExoDNA** creatures cannot have a  technology stat and must be made to have a compassion of at least 11. |

 

## Prime Matter?

If creating life was not enough, it is believed (though others say it’s crazy) that the **Ancient Fusion** for creating **Prime Matter** can be unlocked by using 3 created lifeforms to fuse all previously mentioned currencies together (all basic resource currencies, all three elite currencies and BioMetal and ExoDNA).

Prime Matter may hold the key to limitless energy and may bring humanity into a new age of prosperity. 

Or maybe the power will be too much to handle. Perhaps the **Prime Matter** is why the **Ancient Space Empire** fell. Still, rash though it may be, many have tried. 

## Moving through the Tree 

**GMs -** Let’s bring it all together. If you are running a game of *Fueled by a Hostile Sun* that focuses on achieving the Prime Matter as its ultimate goal, the players win if they create three beings and thus can do the fusion. To make three beings they will need (at least) three units of divine currency. 

This will require the production of the proper Elite Currencies, both to make the Divine Currencies and to unlock the ability to access the most dangerous worlds. These worlds are where they will find the necessary creatures to make divine currencies. 

**The Currency Tree Table on the next page summarizes the steps in the currency tree starting with the basic currencies and moving up to the Divine currencies.** This can serve as a helpful “quest window” guide for the players. 

# Currency Tree 

| **Campaign Goal**                                      | **Steps**                                                    |                                                     |                                               |
| ------------------------------------------------------ | ------------------------------------------------------------ | --------------------------------------------------- | --------------------------------------------- |
| **[1] Make Elite Currencies**                          | Tame Alien Creatures that can  convert resources.            | 2 OO + 2 MM makes **Mithril**                       | 3 SS + 3 AA makes **Mutagen**                 |
| **Make Elixir**                                        |                                                              | 4 HH makes **Merciful Elixir**                      |                                               |
| **[2] Make Divine Currencies**                         | Befriend Legendary Alien  Creatures                          | 2 Mithril +1 Elixir = 1 **Living  Metal**           | 2 Mutagen +1 Elixir = 1 **ExoDNA**            |
| **[3] Create Life**                                    | Find Ancient Recipes                                         | Living Metal + Ancient Blueprint*  = a **Superbot** | ExoDNA + Ancient Fossil* = a **Reborn  Life** |
| **Find the Prime Matter to Unlock  Limitless Power??** | Create 3 life. Once you make 3 you can use them to forge  the **Prime Matter** by fusing one unit of each of the above resource  currencies. |                                                     |                                               |

*****Found as a narrative plot point during the campaign.

**Blockchain:** Faster-Than-Light Cryptocurrency (which, sadly, the universe has succumbed to using) equivalent to the **universe-wide average economic cost** of one of the basic currencies. The local cost can be higher.

It costs 2 Blockchain to invent a potion recipe (see the next chapter on Alchemy.

## Alchemy

The basic resources each have a basic use, but players will want to explore alchemy beyond these simple uses. 

This system is intended to allow the players to make most anything, whilst having reasonable guardrails. 

- If players want a **one-use effect** they have wide leeway, the limiting factor being that the one-use potions are the most expensive per-use. (p34)
- If players want **multiple uses**, they are committed to using the potion with a **specific stat** for a specific situation. (p35)
- If the players want a pet mutation or a machine that will be a **persistent part of the game,** they will be limited by a **pet’s food cost** or a machine’s **refueling costs**. 

The following charts can guide GMs in knowing how much to charge in resources for a potion.

Each class can learn a different number of recipes. Recipes can be forgotten at will to make room to learn new ones. Inventing a recipe costs 2 Blockchain. 

**Game Designer’s Note:** Pets and inventions may seem similar mechanically, but there are key differences. Remember, a pet must be fed its **full** food cost to get its stat pool back. Conversely, an invention can be charged by one unit, if desired. Pets can move and act autonomously whereas inventions must always have an operator.  The drop rates of the Basic Resources can make mithril harder to produce than mutagen. Pet’s need to be tamed before they can be mutated. Inventions can be made from scratch.

## My Potion Is a One-Use Effect

This potion type gives players lots of leeway as to the potion’s effect, but it’s one use, making it the most expensive per use. 

| **Traits  of Potion**     | **Base**  **Price** | **Base**  **Uses** | **Explanation**                                              |
| ------------------------- | ------------------- | ------------------ | ------------------------------------------------------------ |
| **Odd Ore Based**         | 3 OO                | 1                  | Creates a structure, possibly  bending the laws of physics. Structure  can be used for 1 round before it breaks down. |
| **Molten Minerals Based** | 3 MM                | 1                  | Creates heat, explosions,  energy, or some other pyrotechnic effect |
| **Strange Sap Based**     | 3 SS   +   1 AA     | 1                  | Biological impact                                            |
| **Arcane Algae Based**    | 3 AA   +   1 SS     | 1                  | Psychological impact                                         |
| **Elixir Based**          | 1-3  Elixir (p32)   | Various            | Quests to cure an alien disease  will often require [1] Finding a proprietary ingredient and [2] Mixing said  ingredient with a certain amount of Elixir. |

  

## Potion Let’s Me Do an Action Better

| **Traits  of Potion**                                        | **Base  Price**                                | **Uses** | **Explanation**                                              |
| ------------------------------------------------------------ | ---------------------------------------------- | -------- | ------------------------------------------------------------ |
| **I can reroll when I use [stat]  for [situation]**          | 3 * [Basic Resource]                           | 3        | Use the Lore for the basic  resources to determine which fits best |
| **Instead of using [Normal Stat]**  **I can use [other stat]** | 2 * [Basic Resource A ] +2  [Basic Resource B] | 5        | Basic Resource A is the resource  mined with the [Normal Stat].   Basic Resource B is the resource  mined with the [Other Stat] |

## Potion Add-On Traits

| **Addon  traits**                                            | **Add  to Potion Price** | **Uses** | **Explanation**                                              |
| ------------------------------------------------------------ | ------------------------ | -------- | ------------------------------------------------------------ |
| **Can be used an additional time**                           | +1 OO                    | +1       | Can add 1 or 2 more uses to  action related potions.         |
| **Works in Plasma**  **Environments**                        | +1 MM                    | N/A      | A stabilizer is needed if the  potion must work in a Plasma environment. |
| **Works in Radioactive  Environments or in contact with water** | +1 AA                    | N/A      | Stabilizer                                                   |

## Grant Mutation to Pet

Players have wide leeway in negotiating the effect of the mutation on their pet, but the effect must be GM approved.

| **Traits  of Mutation Protocol**                | **Price**                | **Uses**                                                     | **Notes**                                                    |
| ----------------------------------------------- | ------------------------ | ------------------------------------------------------------ | ------------------------------------------------------------ |
| **Gives Permanent**   **Mutation to Tamed Pet** | 2 Mutagen + [Pet’s Food] | A pet  must use up one of the points from its stat pool (any stat) to use its  mutation. | **A mutated pet counts as two pets**  against the  PC’s pet stat.     Feeding restores the Pet’s stat  pool, and thus its ability to use its mutation. |

 

## Creation Is a Rechargeable Invention

| **Traits  of Invention**    | **Base  Price**   | **Uses  before Refuel**          | **Notes**                                                |
| --------------------------- | ----------------- | -------------------------------- | -------------------------------------------------------- |
| **Mithril Based Invention** | 2-4 Mithril (p32) | 1 (But  can add more uses below) | Each invention counts as a “bot”  against your bot stat. |

 

## Invention Add-Ons

| **Traits  of Invention**   | **Add  to Price** | **Uses  before Refuel** | **Notes** |
| -------------------------- | ----------------- | ----------------------- | --------- |
| **Increase Fuel Capacity** | +1 MM             | +1                      |           |
| **Work Underwater**        | +1 AA             | N/A                     |           |

 

## Fuel

| **Traits  of Potion** | **Fuel  Price** | **Fuel  Produced** | **Notes**                                      |
| --------------------- | --------------- | ------------------ | ---------------------------------------------- |
| **Fuel Recipe**       | 2 SS            | +1                 | Bots / Inventions can use either  fuel recipe. |
| **Fuel Recipe (alt)** | 1 MM            | +2                 |                                                |

# Alien Creatures

Lifeforms encountered in a hostile environment are **tamable**  To tame a creature, the party will need to (1) avoid offending its triggers (if any, determined by narrative), (2) give a **species-specific gift**  and (3) depending on the species’ **social pattern** use compassion or dominance to tame (generally one action, 3 rolls, 2 of which must be successes).  

Example creatures can be found on p68.

**Furthermore,** you must not go over the number of pets you are allowed to own at one time, as set by your archetype’s pet stat. **Pets bond to the party member who tamed them and cannot be transferred.** They can, however, be returned to the wild, stronger for the new skills they have learned. 

**Game Designer’s Note:** These restrictions are meant to help drive turnover in the pets owned by the party so that new creatures can constantly be in the narrative.

There are, rumors say, some bots still functioning from the Ancient Space Empire. These can be tamed too, using the Tech stat (or Dominance for Smugglers), though many bots will be purchased by the party with Blockchain. 

Attempting to tame is a single action, but the gift is consumed even on fail. Tamed lifeforms stay on their planet and don’t travel in space generally.

## Creatures in Hostile Environments

While the Pet Stat on a character’s archetype chart shows how many pets they can own, the party as a whole can only bring a number of Pets equal to # of People in Party +2 into a Hostile Environment. Other pets can be left at camp to make resources or rest.

## What Can Creatures Do?

1. Some lifeforms **grant a buff or buffs** to its owner when they are with them. 
2. Some lifeforms, if left in camp, can spend a day to **produce Mithril/Mutagen/M. Elixir** etc if given the correct materials. (as discussed in “Resource Currencies and Wealth”). This will be the main way of getting elite resources. A pet left in camp does not provide its buff to its owner. 
3. Some creatures **provide a stat pool** that their owner can draw on if they are with them. One stat point can be spent to reroll a die roll made with that stat or to change the first result on a 1 to 1 basis.

**Game Designer’s Note:** Say Gary has a Might of 13 and needs to clear some rubble. His pet, an Iron Serpent, has a Might Stat Pool of 2. Gary rolls a 15, which would normally be a failure. If succeeding is important enough to him, he can spend those 1 of those 2 points to reroll. If it is very important, he can spend both points to turn the 15 to a 13.

Each creature description states what resource currencies (and what amount) it can use as food. Paying that certain amount of the food resource refreshes the stat pool. The full food requirement must be given to replenish a pool, even if the pool is not fully depleted.

## What do Characters Know About Creatures They Just Met?

Many common creatures are known by explorers across the galaxy. However, if the characters have not encountered a creature before, the GM can use one of the following options to decide what the players would and would not know.

For efficiency an NPC could debrief the characters on new lifeforms on the planet they are exploring.

The GM can permit the following three facts be known after a **single round’s observation:** [1] With a round’s observation players can determine the likelihood a creature is likely to become hostile, based on behavior [2] Players can determine which stats the creature has stat pools in. [3] How many resources the creature would want as a gift for a tame attempt to be possible. Which resources? Well:

The GM can allow players to reasonably presume that **creatures prefer gifts of resources that are hard to find**. A creature in a volcanic environment is unlikely to have access to the benefits of Arcane Algae. Thus, an AA gift will entice, a MM gift will bore. This can allow players to discern what gift is necessary to initiate a tame attempt.

Or, the GM can assign the party as a whole a **Biology Knowledge** stat equal to 5+ [Combined Level of all players]+[Number of Creatures Currently Owned by party] with a maximum value of 17. If a player encounters a new creature, a **Biology Roll** can be called for and on a success the players can discern the characteristics of the beast they just met.

## Common Bots

Bots, like pets, are **coded to a specific owner**, **but they are easier to transfer between owners.**  It takes 1d3 days to change all protocols and teach control methods to a new owner. 

The below are common temporary-use bots. Some bots function more akin to creatures and some examples are in the sample creatures list ahead (p68). 

- **Drone Bot** – allows for investigation rolls to be done by user for squares where the user is not. These bots can have a movement of 1-4 and have a cost in blockchain that matches their movement. 
- **Helper Bot** – Bot increases a stat by +4 (max 18) when doing a specific action – costs 3 Blockchain, has 3 uses. 
- **Elite Helper Bot** – Increases all stats less than 15 by +2 – costs 4 blockchain, can be used as many times as desired in **two** hostile environment scenes. 
- **Specialist Bot** – Bot increases a specific stat (all actions) by +3 – costs 4 blockchain, has 4 uses.

# Open Exploration & NPCs 

## Open Exploration

During open exploration if success or failure is in doubt, the GM can decide which stat best fits the situation. **Players will have more leeway to invent situation specific solutions and will not necessarily need to primarily draw on predefined moves.**  

If you are familiar with exploration in other systems, similar is done here. **As always,** **to test a stat roll equal or under the appropriate stat on a d20.** In open exploration, it may be ideal for the GM to limit the number of dice rolls and allow freer roleplay and creativity.

Often exploration will be in space stations with a life support system. There players will meet NPCs and need to **bargain** with them. The **keywords** on the next page can give the GM a framework to handle these diplomatic interactions. 

## Diplomatic Interactions (Optional)

Each player will choose one **keyword** from each set below. The first sets are the players’ choice, the final keyword must match the higher of Compassion or Dominance. 

**Peasant**  /  **Merchant**  /  **Criminal**  /  **Noble**

**Traditionalist**  /  **Autonomist**

**Private**  /  **Moderate**  /  **Spotlight**

**Compassionate / Dominant / Balanced** (balanced can be picked if Compassion and Dominance are equal or no more than 1 point different)

The future world has regressed to a very class-divided society.  Local governors’ control (and oppress) most persons in society. People often turn to space exploration to improve their station. These keywords reflect where your character falls in the class divides of society. 

When designing NPCs, the GM will note what **keywords** the NPC is biased in favor of, and which against. If pro-biases outnumber anti-biases, it will be easier to negotiate with the NPC. If the reverse, the NPC may only be amendable with a bribe or with a successful Compassion / Dominance roll. 

# Being the Great and Merciful Galactic Master (GM)

## Running Your First Game

The quick reference on itch.io summarizes most of the rules you will need.

One important pitfall to avoid: It is important to note that certain environments will be harder or easier for certain characters. Make sure when you design your scenarios that **there is a use for each character in them, so no character feels like they are useless.** 

So, if there is a character that is weak in resisting the current hostile environment, there should nonetheless be tasks in that environment that the character is good at. Say a character is weaker at resisting diseases, but strong at tech. They may take lots of damage, but the party may need to keep them alive to salvage certain critical ship parts. 

Having to move a character through an environment they are weak against can be a good teamwork challenge, if the players strategize.

Another pitfall to avoid is running environment maps that are too similar. Each scene / environment in your story should present the player with different goals.

## Keeping Exploration Interesting

A good exploration scene:

- **Has choices** – players should be able to choose to pursue different clues or resources based on what they think  is important. 
- **Be possible** – think how many movements it takes to get to places on your map. As you will see in the sample hostile environments below, that is likely the minimum number of times they will be in danger of losing stamina. Are the odds exciting but achievable? 
- **Has narrative weight** – The clues found will be most interesting if you have a mystery that impacts the lives of their characters and thus has meaning. Or if the scene is pursuing resources the finding (or not) of those resources should have narrative weight.
- **Vary mechanics** . . . see below.

## Unusual Exploration Ideas

- **The Maze** – the players are exploring ruins from the Ancient Space Empire. On each turn the maze changes slightly, making the decision to go further in a risk . . . but here is treasure to be found.
- **Rescue Mission** – The players are attempting to save people. Once they remove a stranded explorer from their immediate predicament the players will have to choose if they immediately go to save the others or if they accompany the ones they already rescued to safety. 
- **The Hunt** – A creature the party wants to tame is shy and is evading them. Given the creatures’ high movement, to get a tame attempt the party will need to devise a way to surround it. 
- **The Vehicle** – the environment is too hostile for players to spend more than two rounds in it. So, to move around, they must have a vehicle they all share. The vehicle has limited mobility and needs to go forward three squares before it is allowed to turn. The extra movement constraints will require strategic choices.
- **Unstable Reactors** – The party needs to loot an abandoned space station, but its reactors are kicking up. Players can spend a tech roll to stabilize a reactor – but that takes precious time from investigating and looting. Players will have to strategize who will focus on collecting loot and who will focus on keeping reactors stable. 
- **Portable Shelters** – the party can only spend two rounds in the environment before they need to be in a portable shelter (or risk heavy stamina damage). They have a limited number of shelters to set up. Players will have to decide how to build a chain of shelters to make the exploration most productive. 



# Ten Sample Hostile Environments

Here are some of the horrors explorers have faced. 

If players run out of opportunity, each player loses 1d8 stamina and runs the risk of losing more each round thereafter.

The environment descriptions ahead list what range risk levels that environment would fall into. If it says Risk Level: 2-3, it means that some areas / worlds with that environment are a Risk Level 2, others are at 3 and the GM should decide what is best for the current narrative.

The Risk Level determines which column of the chart on p25 tells the size of the opportunity die. 

Effects labeled “**On scene start**” only happen only once and are rolled before anything else happens in the scene. Effects that impact “**all players in environment**” take place when the character takes their turn. 

**Elevated impact effects** take place when a player takes their turn, starting in an elevated impact square or first enters an elevated impact square on their turn. (Moving through multiple elevated impact squares does not cause the effect to fire again). 

**Special Area** effects only happen if a player accidentally disturbs or enters the special area.

## Acidic Swamp

- **Risk Level:** Risk level 1
- **On Scene Start:** Disease resist or -1 to all rolls during the scene.
- **All Players In Environment:** Disease Resist needed or else the fumes disorient the PC and they move 2 squares in a random direction.
- **Elevated Impact Spread:** Pre-placed slime blobs expand in all directions by one square, stop growing after three expansions. Areas with blobs are elevated impact for remainder of the scene. 
- **Elevated Impact Effect:** Test Might to escape blob, on failure -1 stamina.

## Artic Planetscape

- **Risk Level:** Risk Level 1-2
- **All Players In Environment:** Elemental Resistance roll needed, else player moved one square downwind.
- **Special Area:**  Thin ice. If walked on test Agility. On fail drops into ice water. -1 Stamina immediately and an additional -1 on activation unless successful elemental resist. 
- **Elevated Impact Spread:** Start with two snow storms (each one square in size). On d% 0-20 they become 2 by 2 squares. On 30-40 an additional storm (one square) is created at a random location on board. Each turn use a d6 to determine which direction the storms travel. Each turn they move one square. 
- **Elevated Impact Effect:** When in a snowstorm. test Elemental Resistance, on a fail – 1 Stamina. Also test Agility. On fail an additional -1 Stamina is lost and your character cannot move this round. 



 

## Cosmic Electric Storm

- **Risk Level:** Risk Level 2-3
- **On Scene Start:** Radiation sickness. Players test Disease Resist else all stats are -1 for remainder of scene. 
- **All Players in Environment:** Elemental Resist needed or else player can chose to take an action or move (not both) or chose to do both action and move but lose 1d6 health.
- **Elevated Impact Spread:** 1d4 random squares become elevated impact due to falling radiation crystals,
-  **Elevated Impact Effect:** Test disease resist. On fail -1d6 stamina. 

 

## Destabilized Mine 

·     **Risk Level:** 3 - 💀

·     **On Scene Start:** Disease Resist or -1 to all rolls during the scene. Unhealthy air. 

·     **All Players In Environment:** No roll for all players in environment.

·     **Elevated Impact Spread:** If a player moves to a square that has not been stepped on yet this turn, there is a 20% chance that square is destabilized. Player must test Agility or else become pinned and lose -2 stamina. Roll this only for the final square in the player’s movement.

·     **Space Rats** A pinned character must pass a Pest Resist each round after they become pinned or lose -1 stamina.  There are some nasty critters in the cave.

·     **Fumed Filled Rooms**: Certain rooms in the mine have fumes. The players can learn a recipe, costing 3 herbs, that allows them to remove all fumes from a room. If a player enters a gaseous room, they must make a Disease Resist or  else they move 1d4 squares in a random direction in confusion. 

## Electric Space Station Wreckage 

·     **Risk Level:** 1

·     **On Scene Start:** Space insects – Pest Save or else no actions for first turn. 

·     **All Players in Environment:** Successful Agility needed or movement is limited to 1 space, due to rubble.

·     **Elevated Impact Spread:** Roll d%.  1**-**45 one area becomes secretly electrified. 46-85% chance two random areas become secretly electrified. 86-100 three random areas become secretly electrified.   

·     **Elevated Impact Effect:** Test Agility, on fail lose 1d6 stamina. 

## Endless StormWorld

·     **Risk Level:** 2-3

·     **On Scene Start:** Roll tech, on fail first two Resist Rolls the character takes are roll-twice-take the worse value. 

·     **Special Area:** Lightning strikes have formed crater-like burrows for electric creatures. On first move in or to an adjacent to a square with a crater burrow, test Pest Resist. On fail, an Electric Locusts Swarm allows you to move or act but not both. 

·     **All Players In Environment:** Elemental resist needed. On fail stamina -1 and tech -1. Tech damage heals at end of scene.

·     **Elevated Impact Spread:** There is a 20% chance that a 2 by 2 square becomes affected by even higher lightning strikes.

·     **Elevated Impact Effect:** Test elemental resistance. On fail -1d6 stamina and -2 tech. Tech damage heals at end of scene.

## Meteor Shower

·     **Risk Level:** 💀 Scenes in meteor showers should be intense and quick.

·     **On Scene Start:** Disease Resist or else lose first turn due to radiation in the meters. 

·     **All Players in Environment:** Elemental resist needed else -1 stamina from impacts.

·     **Elevated Impact Spread:** There is a 40% chance that you will select a player at random. 

·     **Elevated Impact Effect:** Selected player must test Agility or take d8 stamina damage when they activate. 

## Microbial World (Semi-Gated)

·     **Gate:** *Diseased* - If the party has not used the Merciful Elixir currency to make a vaccine, then upon entering the microbial world they get ill, causing all stats to decrease by 5 until they rest for a week away from the microbial world. 

·     **Risk Level:** 2-3

·     **All Players In Environment:** Disease Resistance roll needed, else player cannot act this round. They continue to lose turns until they roll a success on the disease roll. Once succeeded against, this does not need to be rerolled.  

·     **Special area:** Toxic slug pit. On stepping in a pit, a Agility check is needed, else player falls in, It takes a full movement to get out. If a player falls in, they must disease resist or lose 1d6 stamina. 

·     **Elevated Impact Spread:** One square has fumes. There is 25% that it will spread in a cone shape. The first spread three new squares are impacted. Thee next causes 5 new squares to be impacted. Then 7 etc., in a cone shape. 

·     **Elevated Impact Effect:** Disease Resist needed, else -1d4 stamina. 

## Plague World (Gated)

·     **Gate:** *Diseased -* As microbial world semi-gated only without vaccine death occurs for the PCs in 1d3 days.  

·     Permanent banishment of the disease will require a story-specific ingredient to be found.

## Second Planet from a Sun (Gated)

·     **Gate:** *Plasma Environment -* Party needs to have suits equipped with Mithril else the heat will kill them instantly. 

·     Run encounter as Volcanic Planetscape (p59) but failing roll in elevated impact causes -3 to stamina, not -2. 

·     **Special Area:** Molten Fleas lie in wait of some of the bushes. If a PC or their pet enter that area a Pest Resist is required. On a fail, their pet can no longer provide the trainer with their buff or stat pool. As am action, an additional Pest Resist can be attempted to end the effect for all pets of that trainer.

 

## Unclean Ocean 

·     **Risk Level:** 1-3

·     **On Scene Start:** Disease resist or -1 to all rolls during the scene.

·     **Special Area:** Ice vents are in certain squares that deal 1d6 damage as an *aquatic chill hazard.*

·     **All Players In Environment:** Elemental Resist or else -1 stamina from cold. If this roll is failed the character also moves two squares downstream unless they subsequently pass a Might check. 

·     **Elevated Impact Spread:** Prior to scene four heat vent locations are placed on map. First round there is a 10%chance one at random activates. This increases by 5% each round. When activated the heat vent square and all adjacent squares are elevated impact. 

·     **Elevated Impact Effect:** Test elemental resistance or else the player loses -1d6 stamina. If player takes this damage, next round they do not need to roll against the cold discussed in “all players in environment.”

## VineworldWorld

·     **Risk Level:** 1-2

·     **On Scene Start:** Jungle insects. Pest Resist or -1 to all rolls during the scene on turns where a movement took place. 

·     **All Players in Environment:** Disease resist needed or else all rolls this turn are at a -1. 

·     **Elevated Impact Spread:** 40% chance of a player being chosen at random. The vines the square the player is in are disturbed causing that square and all adjacent squares to become elevated impact for rest of the scene. 

·     **Elevated Impact Effect:** Test Agility or else not able to move. If unable to move when in an area of activated vines (even if it isn’t the vines that are immobilizing you) roll pest resist. -1 stamina on fail. 

## Volcanic Planetscape

·     **Risk Level:** 1-3

·     **Special Area:** Unstable mineral deposits may explode in certain squares. These deal 1d8 damage as a *crystalic fire hazard.*

·     **All Players in Environment:** Elemental resistance roll needed, else player can either move or act, but not both. 

·     **Elevated Impact Spread:** 2 random squarees per round have a heat burst making that square and the surrounding elevated impact for the round.

·     **Elevated Impact Effect:** Text Agility, on failure -2 stamina, in addition to normal impact. 

## ZenoRadioactive Ruins (gated)

·     **Gate:** *Zenoradiation -* Party needs to have suits equipped with mutagen else the zenoradiation will kill them instantly. 

·     **Risk Level:** 2-3

·     **All Players In Environment:** Disease resistance roll needed, else player can either move or act, but not both. Furthermore, any rolls this round are at a 2-point penalty. This stacks. Players can prepare a potion costing 2 Merciful Elixir that negates this effect for the entire party.

·     **Elevated Impact Spread:** One random line of squares each round becomes elevated impact.

·     **Elevated Impact Effect:** Test elemental resist or else 1d4 stamina lost on first fail. Each subsequent fail (on other turns) causes the die size to increase. So the second fail would be a d6, the third a d8 . . . 

 

## War-torn Terrain

·     **Risk Level:** (No Opportunity die needed but note description of ‘All Players in Environment’ – there is a built in time limit.)

·     **Special Area:** Crater rat nest. On disturb requires pest save else -2 stamina.  

·     **All Players In Environment:** Disease resist needed else -1 Might for remainder of scene due to fumes from abandoned weapons. Roll is repeated every round. At zero Might a player dies. Might recovers after leaving war-torn terrain.

·     **Elevated Impact Spread:** Before scene mine locations are predetermined.  

·     **Elevated Impact Effect:** Test Agility, on fail lose -3 stamina. 

 

# Pre-Made Potion List

| **Molten Minerals  Based** | **Name**             | **Brew Time** | **Effect**                                                   |
| -------------------------- | -------------------- | ------------- | ------------------------------------------------------------ |
| **2 MM + 2 AA**            | Inner Heat           | 2 hours       | Makes user  immune to *Aquatic Chill** hazards for five rounds |
| **2MM +2 SS**              | Feral Link           | 4 hours       | When trying to  tame an animal through dominance, you can re-roll one failed roll |
| **3 MM**                   | Heat Call            | 0.5 Hour      | Can serve as a Lure  for Dragons (who are not impacted by normal lures) |
| **2MM + 1 OO**             | Thick Skin           | 4 Hours       | User gains 1d6  temporary stamina points                     |
| **2 MM + 2 OO**            | Armor  Reinforcement | 8 Hours       | Make armor  immune to *Crystalic Fire* Hazards               |
| **3 MM + 1 HH**            | Adrenaline  Boost    | 1 Hour        | Take to reroll  a failed Might roll. (3 Uses)                |
| **3 MM + 1 SS**            | Fiery Will           | 1 Hour        | Take to reroll  a failed focus roll. (3 Uses)                |
| **2 MM + 2 AA**            | Acid                 | 0.5 Hour      | Make an acid  that can burn through most metals (but not mithril enforced armor) |
| **2 MM + 1 AA + 1  SS**    | Shockwave            | 0.5 Hour      | Creates a  shockwave that can scare animals and cause most computer systems to  malfunction. |
| **2 MM**                   | Smoke Bomb           | 0.5 Hour      | Produce a smoke  bomb. Will be fled by Mutant Bees.          |

 

*Words in italics are keywords that the GM can chose to associate with a hazard, trap or obstacle. 

| **Odd Ore Based**      | **Name**            | **Brew Time** | **Disease Resist**                                           |
| ---------------------- | ------------------- | ------------- | ------------------------------------------------------------ |
| **2 OO +3 AA**         | Unsinkable Rock     | 1 Hour        | Rock when  activated will rise, carrying user to surface. Can also be used as a stepping stone over  water. |
| **2 OO +3 SS**         | Hallucinatory  Rock | 4 Hours       | Rock breaks,  producing mist. Subject (unless makes  successful Focus Roll) is distracted for an hour, in hallucinations. Great for distracting station leaders,  though they will be angry later. |
| **3 OO**               | Portable Pillar     | 4 Hours       | On command rock  can grow into a 12’ high and 4 foot diameter pillar. The pillar does not revert, and is made of  heavy solid stone. |
| **1 OO +1 MM +  1SS**  | Reinforce Pick  Ax  | 1 Hour        | Can on 3  occasions, reroll failed rolls related to mining from rock or collecting MM  or OO |
| **2 OO + 1HH + 2  SS** | Fake Egg            | 3 Hours       | Creates a fake  egg that real animals may take back to their nest. It is large enough for a brave soul to fit  inside. |
| **2 OO + 1 SS**        | Sturdy Foot         | 2 Hours       | Allows use to  reroll 3 Agility rolls related to keeping one’s balance. |

 

| **Strange Sap Based**   | **Name**                 | **Brew Time**     | **Effect**                                                   |
| ----------------------- | ------------------------ | ----------------- | ------------------------------------------------------------ |
| **3 SS**                | Wind Sail                | 8 Hours           | For 3 rounds  the player has a flight speed of 5             |
| **2 SS + 2 AA**         | Invisibility             | 2                 | User is  invisible for 3 rounds                              |
| **3 SS**                | Swap Traits              | 8 Hours           | For up to five  rounds a pet can have the traits (whichever you choose) of another pet owned  by a party member |
| **2 SS + 1 AA**         | Wood into Food           | 3 Hour, 50%  fail | Converts a tree  into edible substance sufficient to get a person through a day. The substance is unhealthy and should only  be eaten once a week to about 3 times a month. It tastes terrible. |
| **3 SS + 2 OO**         | Gas Bubble               | 2 Hours           | *Noxious Gas* hazards have  no impact on the user or anyone within 3 spaces of them for three  rounds. |
| **2 SS + OO + 1  HH**   | Smell Like a Mutant  Bee | 1 Hour            | Removes the  effects of bee poison and allows safe access to a giant mutant bee hive. |
| **2 SS + 2 OO**         | Shrink                   | 4 Hours           | Shrink down to  10% of your normal size for 1 hour.   After 1 hour there is 25%  chance you do not change back. Rebrew  the same potion to unshrink. |
| **2 SS + 1 AA + 1  OO** | Massive growth           | 2 Hours           | Cause plants to  in aera to have a sudden massive growth spirt, above what is natural for that  plant species. |

  

| **Helpful Herbs  Based** | **Name**                   | **Brew Time**              | **Effect**                                                   |
| ------------------------ | -------------------------- | -------------------------- | ------------------------------------------------------------ |
| **3 HH**                 | Antidote –  General Poison | 1 Hour                     | Cures general  poisons of levels 1 and 2                     |
| **2 HH + 2 SS**          | Clear Mind                 | 1 Hour                     | Gives someone  under mind effects 3 rounds of perfect mental clarity. They cannot be hurt by mind effects during  this time |
| **3 HH + 1 OO**          | Mighty Mold  Kill          | 2 Hours                    | Cures effects  of *Exhaust Mold Infection* (can be placed by GMs as a trap in rotting  environments, Disease Resist to negate if touched, -2 to all stats until  cured) for one person or an area bout the size of a large car. |
| **2 HH + 2 AA**          | Cosmic  Antiviral          | 4 Hours                    | Cures effects  of *Warp Virus*                               |
| **3 HH + Special**       | Cure                       | Various,  narrative driven | Diseases may  require a special ingredient to be added to a concoction of healing herbs to  be cured |
| **2 HH**                 | First Aid                  | 0.5 Hour                   | Heal d6 wounds                                               |
| **2 HH + AA**            | Higher First  Aid          | 1 hour                     | Heal d10  wounds, only one of these can be used in an hour on a given patient |

 

| **Arcane Algae  Based** | **Name**          | **Brew Time** | **Effect**                                                   |
| ----------------------- | ----------------- | ------------- | ------------------------------------------------------------ |
| **2 AA + 2 SS**         | Innsmouth Tonic   | 4 Hours       | Have working  gills for one hour                             |
| **3 AA**                | Soap Bridge       | 1 Hour        | Create 12 foot  long bridge of foam over water that can be walked on. |
| **2 AA + 1 MM**         | Acid              | 16            | Create a flask  of acid that can burn through most substances |
| **2 AA + 1 HH**         | AntiGrease  Sheen | 1 Hour        | User is immune  to effects of *Aquatic Toxic sludge for 4 rounds* (as Mold Infestation,  p61 but for Aquatic Environments) |
| **2 AA + 2 SS**         | Agility boost     | 1 hour        | Allows user to  reroll 3 failed Agility throws.              |
| **3 AA + 1 OO**         | Anti-allergen     | 1 Hour        | User is immune  to effects of *Venus Tree spore* for 4 rounds (as Mold Infestation or  Aquatic Toxic Sludge, but is -4 to all stats until cured) |
| **4 AA**                | Floating Bubble   | 0.5 Hour      | Create giant  bubble strong enough to carry a person inside. Will travel 7 squarees a round in the  direction the wind is blowing until popped.   Successful Might required to pop. |
| **2AA +1 SS**           | Superswim         | 3 Hours       | Swim up to 15  MPH.                                          |

 

## With Elite Resources

| **Elite Resource  Based**  | **Name**       | **Brew Time**              | **Effect**                                                   |
| -------------------------- | -------------- | -------------------------- | ------------------------------------------------------------ |
| **2 Mithril**              | Reinforce Suit | 2 Week                     | Space suits (3)  can now enter *Plasma* environments.        |
| **2 Merciful Elixir**      | Inoculation    | 1 Day                      | Allows safe  passage for 3 PCs through an area infected by disease. |
| **2 Mutagen**              | Flexible DNA   | 1 Day                      | Allows safe  passage for 3 PCs through areas of *Zenoradiation* |
| **[Various]  Mutagen**     | Various        | Various,  narrative driven | Evolve a pet to  get a GM-negotiated effect.                 |
| **[Various]**  **Mithril** | Various        | Various,  narrative driven | Upgrade a bot  to get a GM-negotiated effect.                |

 

 



# Tamable Creatures Charts

Charts of resource producing creatures follow.  The left-hand side shows what stats creatures of that type tend to have pools in. Number of points in pools tends to be roughly equal to the number of resources spent to tame/feed. (GM can vary how the points are allocated between different members of the same species.) 

|                                                              | **Name**                                               | **Tame Cost**                                                | **Buff to Owner**                                            |
| ------------------------------------------------------------ | ------------------------------------------------------ | ------------------------------------------------------------ | ------------------------------------------------------------ |
| **Mithril Producing**  (Can attempt  tame after gift, with **Dominance for creatures or tech for bots,**  best 2 of 3**)**     Mithril  producing creatures tend to have stat pools in Might or Dominance     Their tame cost  can be used as Food to replenish their stat pools, or an equivalent amount of  MM or OO. | **Stone Golem (Bot)**                                  | 3 AA                                                         | Can point out  areas of weak rock                            |
|                                                              | **Crystallic Lion (Bot)**                              | 3 SS                                                         | When with Lion,  Dominance +2                                |
|                                                              | **Liquid Flame**  (a living perpetually burning  ooze) | 3 AA                                                         | Owner can can  roll twice and take better when dealing with cold |
|                                                              | **Crater Wolf**                                        | 2 MM + 1AA + 1X                                              | +1 to all stats  that are twelve or less and +1 movement.    |
|                                                              | **Crater Wolf Pack Leader**                            | 5X, chosen by wolf, must already  have a crater wolf to tame, or have done favor for the pack. | +2 to all stats  that are 12 or less and +3 to all stats that are 7 or less. +2 movement. Allows one-reroll a scene. |
|                                                              | **Microbot Swarm (Bot)**                               | 1 HH + 1 MM + 1 SS + 1 AA                                    | First 2 points  of stamina loss are ignored.                 |
|                                                              | **Name**                                               | **Tame Cost**                                                | **Buff to Owner**                                            |
| **Mutagen Producing**  (Can attempt  tame after gift, with **Dominance or Compassion, or tech for bots.** One  action best 2 of 3 rolls**)**     Mutagen producing creatures tend  to have stat pools in either Focus or Agility     Their tame cost  can be used as Food to replenish their stat pools, or an equivalent amount of  SS or AA. | **Xeno Spinster Spider**                               | 3 HH                                                         | Can make 15  foot long web bridges, as well as give a square a hidden web trap. |
|                                                              | **Mutant Bee**                                         | 2 SS                                                         | Allows access  to Queen Bee Hive.                            |
|                                                              | **Giant Arachnid**                                     | 3 MM                                                         | Sticky feet -  Owner can roll twice and take better to keep balance |
|                                                              | **Leviathan**                                          | 3 OO                                                         | Can carry party  across large oceans.                        |
|                                                              | **Multi-eyed Squid**                                   | Any 4, that are not AA                                       |                                                              |
|                                                              | **Ancient Mutabot (Bot)**                              | 2 MM +2 OO                                                   |                                                              |
|                                                              | **Morphing Dragon**                                    | Any 4 that are not MM                                        |                                                              |
|                                                              |                                                        |                                                              |                                                              |

 

 

|                                                              | **Name**                                       | **Tame Cost** | **Buff to Owner**                               |
| ------------------------------------------------------------ | ---------------------------------------------- | ------------- | ----------------------------------------------- |
| **Elixir Producing**  (Can attempt  tame after gift, with **Compassion** best 2 of 3**)**     Elixir  producing creatures tend to have stat pools in either Focus, Agility or  Compassion.     Their tame cost  can be used as Food to replenish their stat pools, or an equivalent amount of  HH. | **Diamond Heart Beetle**                       | 3 MM          | Heal 1d10 /day                                  |
|                                                              | **Stardust Butterfly**                         | 2 HH + 1OO    | +1 to movement                                  |
|                                                              | **Sapphire Squid**                             | 3 OO          | Breath under  H2O                               |
|                                                              | **Furball Lightning**  (Flying Electric Mouse) | 1 HH +2SS     | +1 to all stats  when in an windy/electric env. |
|                                                              | **Ancient Medical bot (Bot)**                  | 3 AA          | Heal 1d8/day                                    |

 

 

|                                                              | **Name**              | **Gift / Food**                      | **Buff to Party**                                            |
| ------------------------------------------------------------ | --------------------- | ------------------------------------ | ------------------------------------------------------------ |
| **Living Metal Producing**  (Befriended,  not tamed, found in areas unlocked with Elite Currency) | **Saturn Falcon**     | **GM provided story-specific favor** | Will provide  players one teleport.                          |
|                                                              | **Living Moon**       |                                      | Will provide a  unique gift (GM choice) that was left on moon prior. |
|                                                              | **Living Comet Dust** |                                      | Can make a  crystal duplicate of any player’s pet.   Same abilities. |

 

|                                                              | **Name**                                                     | **Gift**                             | **Buff to Party**                                          |
| ------------------------------------------------------------ | ------------------------------------------------------------ | ------------------------------------ | ---------------------------------------------------------- |
| **ExoGenes Producing**  (Befriended,  not tamed, found in areas unlocked with Elite Currency) | **Cosmic Eye**                                               | **GM provided story-specific favor** | Will truthfully  (if cryptically) answer any one question. |
|                                                              | **Ancient Pharmacological Bot**                              |                                      | Can increase recipes  known by 2                           |
|                                                              | **Hive Queen** (players will  not be granted audience unless a worker bee is with them) |                                      | Can grant  players mental telepathy permanently.           |

 

The pages that follow provide lore on the five Basic Resources. If desired, the side effects listed can be used as an optional rule when a die roll fails despite being assisted by a potion containing those resources.

## Molten Minerals

**Molten Minerals** - forged in the heat of extreme atmospheres and in the cores of alien planets, Molten Minerals contain much internal latent energy. 

Potions that produce explosions, heating effects almost always include Molten Minerals. 

**Used For:**  Player designed potions involving heat, explosions or large amounts of energy. May be required to keep potions stable in hot and cold environments.

**Drawbacks:** Potions made with Molten Minerals tend to not work underwater

**Side Effects** – If an action fails, even if assisted with a Molten Mineral recipe, often there is an explosion causing 1d4 stamina loss to user. 

 

## Odd Ore

**Odd Ore** – Found in mountainous areas, caverns, mines and in places where Molten Minerals have solidified for a long while.  

These charged substances prove that energy, matter and volume are intercommunicative. In alchemy Odd Ore is used for potions that need to create weight, volume, structures or provide reinforcement. Odd Ore potions can and will break the physics Law of Conservation of Mass.

**Used For:**  Player designed potions that require the Law of Conservation of Mass to break, or the production of structures, or to reinforce materials. Allow can give potions more uses,

**Drawbacks:** Odd Ore is rarely an option for Party-wide potions. Odd Ore alchemy creates an immovable object generally and can only be used by those in the location of that object.

**Side Effects** – If a structure collapses it may pin the user, requiring a Might check to get free. 

## Arcane Algae

**Arcane Algae –** Proof that even across the cosmos water is key for life – and that the seas offer strange secrets. Illusions are often crafted from Arcane Algae and sometimes the more surreal alchemical potions are made with these. 

Often the mutations caused by Strange Sap are encouraged by Arcane Algae. 

**Used For:**  Player designed potions that have a thematic connection to the Sea, involve illusions, influencing creatures or are surreal. Often used with Strange Sap for mutations. 

**Drawbacks:** Arcane Algae potions often don’t work in volcanic environments and some are most useful underwater.

**Side Effects** – Side effects from failed rolls assisted with Arcane Algae potions often take the form of temporary chemical-induced insanities. A mild failure may make a character paranoid for the rest of the scene, a large failure may cause the character to have delusions or visions.

## Strange Sap

**Strange Sap** – Plant life that lives in unstable environments only survives by constantly evolving – sometimes changing its internal genetic code faster than the rate it grows. 

Strange Sap is used by alchemists to increase the adaptations of their pets or to produce new ones, sometimes even grow limbs in their pets or themselves.

**Used For:**  Player designed potions that are meant to provide a temporary mutation to themselves or a pet. 

**Drawbacks:** Mutations are often a two-edged sword. A mutation that makes a player character fireproof may make them more easily damaged by cold, etc. A character that has lost half their stamina cannot (safely) mutate.

**Side Effects** – Side effects from failed rolls assisted with Strange Sap potions often take the form of temporary mutations gone array. The GM will be able to invent consequences based on the attempted mutation. 

## Helpful Herbs

**Helpful Herbs –** the key ingredients of any doctors toolchest. Knowing the difference between life giving roots and death dealing toxins is a valuable asset for space crews. 

Helpful Herbs are the most specific in their uses – namely they are almost exclusively used in medical applications. NPC given quests to find a cure will often involve stockpiling Helpful Herbs for experiments and the like

 

**Used For:**  Medical cures, almost exclusively.

**Drawbacks:** Are rarely used to assist non-medial actions.

**Side Effects** – Quest Specific – depending on the nature of the cure sought. Sometimes the cure will be guaranteed once a narrative-specific ingredient is found.

# Optional Rules / Sub Systems

## Space Travel Times

At the center of the galaxy there is a set of hyperdense blackholes, so strong in their power that they warp the laws of physics and give off the quantum energies that make faster-than-light travel possible. 

Different regions of the galaxy have different levels of radiation from these blackholes.

The red area above has the highest radiation. The green area has a middle amount and is the most livable. The yellow areas (around the green and in between the green and red) have just a bit too much or too little. 

Travel from an area in a given color, to an area in the same color is easier and quicker than going to a different energy level.

Travel times are determined by rolling a **travel die** which determines the number of days, weeks, or months the travel takes, depending on how well mapped the route is. 

Space Travel times can be determined according to the following charts:

| **Energy Level (start)** | **Energy Level (End)** | **Travel Die** |
| ------------------------ | ---------------------- | -------------- |
| **Green**                | Green                  | D6             |
| ""                       | Yellow                 | D8             |
| ""                       | Red                    | D12            |
| **Yellow**               | Green                  | D10            |
| ""                       | Yellow                 | D8             |
| ""                       | Red                    | D12            |
| **Red**                  | Green                  | D12            |
| ""                       | Yellow                 | D10            |
| ""                       | Red                    | D12            |

 

| **Well Mapped?**                        | **Travel Die Reveals** |
| --------------------------------------- | ---------------------- |
| **Very – Lots of Space traffic**        | Days                   |
| **Moderate- Decent amount of  traffic** | Weeks                  |
| **Not well Mapped**                     | Months                 |
| **Undiscovered Area**                   | (Driven by Story)      |

 

Furthermore, if your starting point and ending point form an angle greater than 90 degrees with the center of the galaxy, double the result of the travel die. (This is to ensure distance does impact travel times, in addition to energy levels) 

## Influence

As explorers grow in fame, they may come to be involved in space station politics. A character or party’s favor with different political factions can be represented with **Influence Meters**. Clue Points that help a political sides’ cause can be turned over to them to gain points (1 to 1 basis) on their corresponding Influence Meter. Doing things that anger the faction will cause the meter to decrease.

In general, the meter will start at 7 and can rise to 18 or drop to zero. If the party wants to **call in a favor** the GM assigns a **favor level** from 1 to 5. The party rolls against their current Influence Meter. On a success they get the favor. On a fail, if their influence meter is higher than 10, they still get the favor, but then the meter drops by the favor level number. If the influence meter is less than 10 the favor is not granted.

*This system is not for diplomacy with specific NPCs* – use the Diplomatic Keywords systems for that. Rather this is for calling in favors that require the approval of Planetary Councils, political groups or perhaps even the Council of Seven.

Another use for Clue Points is to have NPCs that will only help the party if a certain number of Clue Points are spent to sway them.

## Heists

Each party member starts out with a **Stealth** equal to their **Agility**. If they are in a position where an NPC may try to spot them, they roll their stealth. On a fail, they lose 1d6, 1d8 or 1d10 stealth depending on how good at detecting invasions the NPC is. When stealth hits zero, they are **spotted.** Read my *Blood in the Night* for more ideas.

## Pet Damage

If a pet is in an environment that it is not well suited for, it can be exhausted as it too must struggle to survive. Whenever the Pet’s trainer takes stamina damage, there is a 50% chance the Pet needs to spend a point from one of its stat pools (PC’s choice) to remain alive. If the pet cannot do this, it perishes.

## Rolling For Stats (Alt. Char. Generation)

If you don’t mind there being variance amongst PCs stats can be generated by rolling 4d6, keeping the highest three dice, and treating 1’s as 2’s. 

Tamed pets can have randomized stat pools as well. If a creature should have a stat pool for a given stat, roll a d6. On evens (except 6), the stat pool =2. On odds, the stat pool =1. And on 6, the stat pool =3. 

## Tougher Stat Pool (Alt. Char. Generation)

For a tougher set of stats, let Stamina, as usual, be set by your Archetype. For the remaining six stats you can use this array 15, 14, 13, 10, 9 & 7. Or you can use a point buy of 68 pts and require that at least three of the stats be 10 or below. 

## Dedication

“Fantasy can, of course, be carried to excess. It can be ill done. It can be put to evil uses. It may even delude the minds out of which it came. But of what human thing in this fallen world is that not true? Men have conceived not only of elves, but they have imagined gods, and worshipped them, even worshipped those most deformed by their authors' own evil. But they have made false gods out of other materials: their notions, their banners, their monies; even their sciences and their social and economic theories have demanded human sacrifice. *Abusus non tollit usum.* **Fantasy remains a human right: we make in our measure and in our derivative mode, because we are made: and not only made, but made in the image and likeness of a Maker.” -** C.S. Lewis

**To C.S. Lewis and J.R.R. Tolkien who understood what good and true fantasy was for and how it could illuminate us.** 

May we never forget. 

 

 